const router = require("express").Router();
const dotenv = require("dotenv");
const requestJson = require("request-json");

const User = require("../model/User");
dotenv.config();

let urlClientes = `https://api.mlab.com/api/1/databases/jucan/collections/testColl?apiKey=${process.env.API_KEY}`;
const clienteMLab = requestJson.createClient(urlClientes);

router.post("/register", (req, res) => {
  const user = new User({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    birthDate: req.body.birthDate,
    email: req.body.email,
    password: req.body.password,
  });

  // This method is a Promise
  clienteMLab.post("", user, (err, resM, body) => {
    if (err) {
      res.status(400).send(err);
      console.log(`Some error: ${resM.statusCode}`);
    }
    res.send(body);
    console.log(`>> Res status: ${resM.statusCode}`);
  });
});

module.exports = router;
